-------------------
| JQUERY SWITCHER |
-------------------

*Intro*

This module allows you to set certain jQuery version for a certain
Drupal path.

Development it is still in development, use with caution.

*Usage*

After enabling modules, go to it's configuration page under
admin/settings/jswitch. There you can add/edit/delete specific jQuery
versions for pages.

*Credits*

Ideas from jquery_update module
(http://drupal.org/project/jquery_update).

Contains jQuery 1.4, 1.5 and 1.6 versions released under MIT and GPL
licenses. Please see http://jquery.org/license/ for more information.
